'use strict';
let express = require('express');
let router = require('./router');//node.js modules to deal with file paths

let app = express();//initalize a vaiable called app with express
let cors = require('cors');

//init middleware

app.use(express.json());// method returns a peice of middleware, to be used in the request process
app.use(cors());
app.use(router);
app.use(cors());

app.get('submit/contact', function(req,res, next){
  res.json({msg:'CORS-enabled'})
})


//error handler

app.use(function(error, req, res, next) {
  console.error(error);
  res.sendStatus(500)//Internal Server Error
});




let port = 7000;
app.listen(port, function () {
    console.log(`Express server started on port ${port}.`);
});

